<?php
  
    include "header.php";
    function alert($msg) {
        echo "<script type='text/javascript'>alert('$msg');</script>";
    }
    if (!isset($_SESSION['admin_id'])){
        header("Location:index.php");
    }
    if(isset($_SESSION['exist'])){
      alert("Sorry, the topic already exist");
      unset($_SESSION['exist']);

    }
?>
<h1 align=center style = "font-size:50px;color:blue;font-family: 'Times New Roman', Times, serif;"> Add New Topic </h1>
<br>
<form action = "addnewtopicproc.php" method = "post">
    <table align=center>
        <tr>
            <td>
               <b> Topic Name: </b>
            </td>
            <td>
            </td>
            <td style = "font-family:verdana;">
               <input type = text class = "form-control" name = "topic" required autocomplete = "off"> 
            </td>
        </tr>
        <tr align=center>
            <td>
            </td>
            <td colspan ="2" >
                <br>
                <button type = "submit" class = "btn btn-primary" style = "width:150px;font-family:Verdana"> Add Topic </button>
                <a href = "topic.php" class = "btn btn-warning" style = "width:150px;font-family:Verdana"> Cancel </a>
            </td>
        </tr>
    </table>
</form>