<?php
    include "header.php";
    if (!isset($_SESSION['admin_id'])){
        header("Location:index.php");
    }
    $topic_id = $_GET['topic_id'];

    $topic = "Select * from topics where Topic_ID = '$topic_id'";
    $topicquery = custom_query($topic);

    foreach($topicquery as $key =>$row){
        $topic_id = $row['Topic_ID'];
        $Topic_Name = $row['Topic_Name'];
    }

?>
<h1 align=center style = "font-size:50px;color:blue;font-family: 'Times New Roman', Times, serif;"> Add New Topic </h1>
<br>
<form action = "updatetopic.php?topic_id=<?=$topic_id?>" method = "post">
    <table align=center>
        <tr>
            <td >
               <b> <span style = "font-family:verdana;font-size:25px;">Topic Name:</span> </b>
            </td>
            <td>
            </td>
            <td>
               <input type = text class = "form-control" name = "topic" required value = "<?=$Topic_Name?>" style = "font-size:20px;font-family:Verdana"> 
            </td>
        </tr>
        <tr align=center>
            <td>
            </td>
            <td colspan ="2" >
                <br>
                <button type = "submit" class = "btn btn-primary" style = "width:150px;font-family:Verdana"> Update Topic </button>
                <a href = "topic.php" class = "btn btn-warning" style = "width:150px;font-family:Verdana"> Cancel </a>
            </td>
        </tr>
    </table>
</form>