<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="assets/img/brain.png">
    <title>Online Quiz</title>
</head>
<body style = "background-color:#F0F6F7FF;">
<?php
   include "header.php";
   if (!isset($_SESSION['admin_id'])){
       header("Location:index.php");
   }
?>
    <div class = "card" style = "width:60%;margin:0 auto;">
        <div class = "card-header bg-primary text-light">
        <h2 align=center style = "font-family:Georgia;font-size:50px;"> Topics </h1> 
           
        </div>
        <div class = "card-body">
        <form method = "Post" action = "addnewtopic.php">
        
            <p align=right> <button type = submit class = "btn btn-success" style = "font-family:verdana;"><i class="fas fa-plus"></i> Add New Topic </button> </p>

        </form>
            <table class="table" style = "width:100%;margin:0 auto;font-family:verdana">
                <thead style = "font-size:30px;" class = "thead">
                    <tr align=center>
                        <th >Topic</th>
                        <th >Popularity</th>
                        <th >Action</th>
                        
                    </tr>
                </thead>
                <tbody>
                <?php 
                $topics = "Select * from topics";
                $topicsquery = custom_query($topics);
                foreach($topicsquery as $key =>$row){
                    $topic_id = $row['Topic_ID'];
                    $topicname = $row['Topic_Name'];

                    $popularity = "Select count(Topic_ID) as Popularity from quiz where Topic_ID = '$topic_id'";
                    $popularityq = custom_query($popularity);
                    foreach($popularityq as $key => $row){
                        $Popularity = $row['Popularity'];
                    }
                ?>
                    <tr align=center>
                        <th><?=$topicname?></th>
                        <td> Played <u> <?=$Popularity?> </u> times </td>   
                        <td><a href = "viewtopic.php?topic_id=<?=$topic_id?>" class = "btn btn-primary" style = "width:70px;font-family:verdana"><i class="far fa-eye"></i>  View </a> <a href = "edittopic.php?topic_id=<?=$topic_id?>" class = "btn btn-warning" style = "width:70px;font-family:verdana"> <i class="far fa-edit"> </i> Edit </a>  </td>
                        
                
                    </tr>   
                <?php 
                }   
                ?>
                
                    
                </tbody>
            </table>
        </div>
    </div>
        
 
    
</body>
</html>
