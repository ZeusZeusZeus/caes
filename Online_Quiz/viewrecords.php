<?php
    include "header.php";
    if (!isset($_SESSION['admin_id'])){
        header("Location:index.php");
    }
    $user_id = $_GET['user_id'];
    $qtype_id = $_GET['qtype_id'];
    $topic_id = $_GET['topic_id'];
    $get = "SELECT * from users where user_id = '$user_id'";
    $getquery = custom_query($get);
    foreach($getquery as $key =>$row){
        $id = $row['user_id'];
        $fullname = $row['Firstname']." ". $row['Lastname'];
    }
?>
<div class = "card" style = "width:60%;margin:0 auto;;text-align:justify-center;">

        <div class ="card-header bg-primary text-light">
            <h1 align=center> <?=$fullname?> </h1>
        </div>
        <div class = "card-body">
    
        <table class = "table" style = "font-family:verdana;font-size:20px;">
            <tr>
                <th>
                    Attempt
                </th>
                <th>
                    Topic Name
                </th>
                <th>
                    Question Type
                </th>
                <th>
                    Score
                </th>
                <th>
                    Date
                </th>
            </tr>
        <?php
            $attempt = 0;
            $quiz = "Select results.score, quiz.Time,  topics.Topic_Name, questions_type.Question_Type from quiz join results on quiz.quiz_id = results.quiz_id join topics on quiz.topic_id = topics.Topic_ID join questions_type on quiz.Question_Type_ID = questions_type.Question_Type_ID where user_id = '$user_id' && quiz.topic_id = '$topic_id' && quiz.Question_Type_Id = '$qtype_id'";
            $quizquery = custom_query($quiz);
            foreach($quizquery as $key =>$row){
                $score = $row['score'];
                $topic = $row['Topic_Name'];
                $Question_Type = $row['Question_Type'];
                $date = $row['Time'];
                $newdate= strtotime($date);  
                $attempt += 1;
                
            ?>
                <tr>
                    <td>
                        <?=$attempt?>
                    </td>
                    <td>
                        <?=$topic?>
                    </td>
                    <td>
                        <?=$Question_Type?>
                    </td>
                    <td>
                        <?=$score?>
                    </td>
                    <td>
                        <?=date('h:i a M d, Y ', $newdate)?>
                    </td>
                    
                </tr>
            <?php
            
            }
            ?>
            </table>
        <?php
        $average = "Select MAX(results.score) as BEST from quiz join results on quiz.quiz_id = results.quiz_id join users on quiz.user_id = users.user_id where topic_id = '$topic_id' && Question_Type_ID = '$qtype_id' && quiz.user_id = '$user_id' ";
         $averagequery = custom_query($average);
         $num = mysqli_num_rows($averagequery);
         if($num!=0){
            foreach($averagequery as $key => $row){
                $AVERAGE = $row['BEST'];
                
            }

         }else{
             $AVERAGE = 0;
         }
       
         
        ?>
        <tr>
         <td>
       
            <h2 style = "font-family:verdana;"> Best Score: <span style = "color:green"> <?=round($AVERAGE)?>  </span> </h2>
         <td>
        </tr>
        </div>

        <h1 align =center> <a href= "viewattempts.php?user_id=<?=$user_id?>&topic_id=<?=$topic_id?>" class = "btn btn-warning" style ="width:150px;"><i class = "fas fa-left-arrow">Back </i> </a> </h1>

</div>