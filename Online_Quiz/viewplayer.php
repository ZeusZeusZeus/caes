
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="assets/img/brain.png">
    <title>Online Quiz</title>
</head>
<body style = "background-color:#F0F6F7FF;">
<?php
    include "header.php";
    if (!isset($_SESSION['admin_id'])){
        header("Location:index.php");
    }
    $user_id = $_GET['user_id'];

    $get = "SELECT * from users where user_id = '$user_id'";
    $getquery = custom_query($get);
    foreach($getquery as $key =>$row){
        $id = $row['user_id'];
        $fullname = $row['Firstname']." ". $row['Lastname'];
    }

?>
    <div class = "card" style = "width:60%;margin:0 auto;">
        <div class ="card-header bg-primary text-white" style = "font-family:verdana;">
            <h1 align=center> <?=$fullname?>'s Quiz records </h1>
        </div>
        <br>

        
            
            
    
        <table class = "table" style = "width:80%;margin:0 auto;font-family:verdana;">
            <thead style = "font-size:20px;" align=center>
                <tr>
                    <th>
                        TOPIC
                    </th>
                    <th>
                        ATTEMPTS
                    </th>
                    <th>
                        View
                    </th>
                </tr>
            </thead>
        <?php
            $topic = "SELECT * from topics";
            $topicquery = custom_query($topic);
            foreach($topicquery as $key =>$row){
                $topic_id = $row['Topic_ID'];
                $topic_name = $row['Topic_Name'];

                $total = "SELECT COUNT(topic_id) as TOTAL from quiz where user_id = '$id' && topic_id = '$topic_id'";
                $totalquery = custom_query($total);
                foreach($totalquery as $key =>$row){
                    $TOTAL = $row['TOTAL'];       
            ?>
                
                <tr style = "font-size:15px;font-family:georgia;" align=center>
                <?php if ($TOTAL !=0) { ?>
                    <td>
                    <?=$topic_name?>
                    </td>
                    <td>
                    <?=$TOTAL?>
                    </td>
                    <td>
                        <a href = "viewattempts.php?user_id=<?=$id?>&topic_id=<?=$topic_id?>" class ="btn btn-primary"><i class="far fa-eye"></i> View </a>
                    </td>

                </tr>     
            
            <?php
                    }
                }
            }
            
        ?>
        </table>
    

        <br>
       
    </div>
    <h1 align=center >
            <a href="javascript:history.go(-1)" class = "btn btn-warning" style = "width:150px;"><i class = "fas fa-arrow-left"> Back </i> </a>
            
    </h1>
</body>
</html>