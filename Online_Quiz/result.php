<?php
    include "header2.php";
    if (!isset($_SESSION['user_id'])){
        header("Location:index.php");
    }
    if(!isset($_GET['qtype_id']) && !isset($_GET['topic_id'])){
        header("Location:userhome.php");
    }
    $_SESSION['submitted'] = 1;
    $qtype_id = $_GET['qtype_id'];
    $topic_id = $_GET['topic_id'];
    

 
    
   


    $question = "Select * from questions where Question_Type_Id = '$qtype_id' and Topic_Id = '$topic_id'";

    $questionquery = custom_query($question);
    $item = 0;
 ?>  
    <div class ="container-fluid" style = "width:60%;margin:0 auto;">
    <h1 align=center> Quiz Result </h1>
    <br>
    <table class = "table table-striped">
        <thead class = "thead thead-dark" style = "font-size:20px;font-family:verdana;font-weight:bold;">
            <tr>
                <th>
                    Item No.
                </th>
                <th>
                    Question
                </th>
                <th>
                    Your Answer
                </th>
                <th>
                    Result
                </th>
            </tr>
        </thead>
<?php
    $SCORE = 0;
    foreach($questionquery as $key => $row){

        $question_id = $row['Question_ID'];
        $question = $row['Question'];
        $item += 1;

        if(isset($_SESSION['answer'.$item])){
            $useranswer= $_SESSION['answer'.$item];
        }

        $cAnswer = "Select Answer from answers where Question_ID = '$question_id'";
        $cAnswerquery = custom_query($cAnswer);
        foreach($cAnswerquery as $key => $row){
            $correctanswer = $row['Answer'];
         
            if ($useranswer==$correctanswer){
                $result = "<p style = 'font-size:18px;font-weight:bold;font-family:verdana;color:blue'> Correct </p>";
                $SCORE += 1;
            }else{
                $result = "<p style = 'font-size:18px;font-weight:bold;font-family:verdana;color:red'>Wrong </p>";
            }
        ?>
          <tr>
            <td>
                <?=$item?>
            </td>
            <td>
                <?=$question?>
            </td>
            <td>
                <?=$useranswer?>
            </td>
            <td>
            <?=$result?>
            </td>
          </tr>
          
               
          
            
        <?php
        }
 
    }
         
   
    ?>
         
    </table>
    <tr align=center>
            <td colspan = 2>
              <h3>  <span style = "font-family:georgia;color:black;font-size:40px;" >Your Score:</span>  <span style = "font-family:verdana;color:blue;font-size:35px;"><?=$SCORE?> </h3> 
            </td>
            <td colspan = 2>
                <h2 align=center> <a href = "userhome.php" class = "btn btn-primary"> Home </a> </h2>
            </td>
            
            
    </tr>
<?php







