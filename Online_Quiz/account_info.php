<?php
    include "header2.php";
    if (!isset($_SESSION['user_id'])){
        header("Location:index.php");
    }

    function alert($msg) {
        echo "<script type='text/javascript'>alert('$msg');</script>";
    }

    if(isset($_SESSION['emailtaken'])){
        alert("Email Update Failed because it is alraedy taken");
        unset($_SESSION['emailtaken']);
    }
    if(isset($_SESSION['usernametaken'])){
        alert("Username Update Failed because it is alraedy taken");
        unset($_SESSION['usernametaken']);
    }
    $table = "users";
    $id = $_GET['id'];

    $data = get_where($table, $id);

    foreach($data as $key =>$row){
        $firstname = $row['Firstname'];
        $lastname = $row['Lastname'];
        $email = $row['email'];
        $username = $row['username'];
        $password = $row['password'];
    }

?>
<div class = "container" style = "width:60%;margin:0 auto;border:none;">
    
    <div class = "card">
        <div class = "card-header bg-info text-white">
            <h1 align=center > Account Info </h1>
        </div>
        <div class = "card-body">
        <table style = "font-size:30px;font-family:verdana;" align=center>
            <tr>
                <td style = "font-weight:bold;">
                    Name:
                </td>
                <td style = "padding:20px;">
                    <?=$firstname." ". $lastname?> 
                </td>
            </tr>
            <tr>
            <td style = "font-weight:bold;">
                   Email:
                </td>
                <td style = "padding:20px;">
                    <?=$email?> 
                </td>
            </tr>
            <tr>
            <td style = "font-weight:bold;">
                    Username:
                </td>
                <td style = "padding:20px;">
                    <?=$username?> 
                </td>
            </tr>
            <tr>
                <td style = "font-weight:bold;">
                    Password:
                </td>
                <td style = "padding:20px;">
                    <input type = "password" value ='<?=$password?>'style = "border:none;" readonly> 
                </td>
            </tr>
            <tr align=center>
                <td colspan=2>
                    <a href = "account_setting_user.php?id=<?=$id?>" class = "btn btn-warning"> Edit account </a>
                </td> 
            </tr>
        </table>
        </div>
    </div>
</div>