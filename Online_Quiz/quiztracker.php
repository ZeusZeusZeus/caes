<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body style = "background-color:#F0F6F7FF;">
<?php
    include "header.php";
    if (!isset($_SESSION['admin_id'])){
        header("Location:index.php");
    }

?>




    <div class = "card" style = "width:60%;margin:0 auto;">
    <h1 align=center class = "card-header bg-primary text-light"> Quiz Tracker </h1>
    <br>
    <div class = "card-body">
    <table class = "table" role = "grid" style = "font-family:verdana;" id = "table" >
        <thead class = "thead" style = "font-size:20px;">
            <tr>
                <th>
                    Name
                </th>
                <th>
                    Quiz Topic
                </th>
                <th>
                    Question Type
                </th>
                <th>
                    SCORE
                </th>
                <th>
                    RESULT
                </th>
                <th>
                    DATE
                </th>
            </tr>
        </thead>
        <?php
            $query = "SELECT quiz.user_id, quiz.topic_id, Firstname, Lastname,Topic_Name, Question_Type, quiz.Question_Type_ID, Time, score from quiz join users on users.user_id = quiz.user_id join topics on quiz.topic_id = topics.Topic_ID join questions_type on quiz.Question_Type_ID = questions_type.Question_Type_ID join results on quiz.quiz_id = results.quiz_id ORDER by Time DESC";
            $quiz = custom_query($query);
            foreach($quiz as $key => $row){
                $user_id = $row['user_id'];
                $topic_id = $row['topic_id'];
                $Fullname = $row['Firstname']." ".$row['Lastname'];
                $Topic = $row['Topic_Name'];
                $Qtype_ID = $row['Question_Type_ID'];
                $Qtype = $row['Question_Type'];
                $score = $row['score'];
                $date = $row['Time'];
                
                $newdate= strtotime($date);  
                
                $pass = "Select COUNT(Question) as Total from questions where Topic_ID = '$topic_id' && Question_Type_ID = '$Qtype_ID'";
                $passquery = custom_query($pass);
                foreach ($passquery as $key => $row){
                    $Total = $row['Total'];
                    $passing = $Total/2;

                ?>
                  <tr>
                <td>
                    <a href = "viewplayer.php?user_id=<?=$user_id?>"> <?=$Fullname?> </a>
                </td>
                <td>
                <a href = "viewtopic.php?topic_id=<?=$topic_id?>"> <?=$Topic?> </a>
                </td>
                <td>
                    <?=$Qtype?>
                </td>
                <td style = "font-size:18px;font-weight:bold;">
                    <?=$score." / ".$Total?>
                </td>
                <td>
                    <?php 
                        if ($score >= $passing){
                            echo "<p style = 'color:blue'> Passed </p>";
                        }else{
                            echo "<p style = 'color:red'> Failed </p>";
                        }
                    ?>
                    
                </td>
                <td>
                <?=date('h:i a M d, Y ', $newdate)?>
                </td>
            </tr>
  
        <?php
                }
           }
        ?>
    </table>
    </div>
    </div>
    

</body>

</html>
    
