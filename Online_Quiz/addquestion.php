<?php 
    include "header.php";
    if (!isset($_SESSION['admin_id'])){
        header("Location:index.php");
    }
    if (!isset($_GE['question_type_id']) && !isset($_GET['topic_id'])){
        header("Location:topic.php");
    }else{
        $topic_id = $_GET['topic_id'];
        $qtype_id = $_GET['question_type_id'];
    }
    

?>
<br>
<form action = "addquestionproc.php?topic_id=<?=$topic_id?>&question_type_id=<?=$qtype_id?>" method = "Post">
<div class = "form-group">
    <table align=center>
        <tr>
            <td> <label> <b> Question:  </b></label> </td>
            <td> &nbsp; <textarea name = "question" rows = "3" cols = "50" style = "border-radius:5px;"  class = "form-control" required ></textarea> </td>
        </tr>
        <tr>
            <td> <label> <b> Option 1:  </b></label> </td>
            <td> &nbsp; <textarea name = "option1" rows = "1" cols = "50"  style = "border-radius:5px;"  class = "form-control" required ></textarea>  </td>
        </tr>
        <tr>
            <td> <label> <b> Option 2:  </b></label> </td>
            <td> &nbsp; <textarea name = "option2" rows = "1" cols = "50"  style = "border-radius:5px;"  class = "form-control" required ></textarea>  </td>
        </tr>
        <tr>
            <td> <label> <b> Option 3:  </b></label> </td>
            <td> &nbsp; <textarea name = "option3" rows = "1" cols = "50"  style = "border-radius:5px;"  class = "form-control" required ></textarea>  </td>
        </tr>
        <tr>
            <td> <label> <b> Option 4:  </b></label> </td>
            <td> &nbsp; <textarea name = "option4" rows = "1" cols = "50"  style = "border-radius:5px;" class = "form-control" required ></textarea>  </td>
        </tr>
        <tr>
            <td> <label> <b>  Answer: </b> </label> </td> 
            <td>&nbsp; <select required class = "form-control" name = "answer"> 
            <option value = "" hidden selected disabled> Select </option>
            <option value = "Option1"> Option 1 </option>
            <option value = "Option2"> Option 2 </option>
            <option value = "Option3"> Option 3 </option>
            <option value = "Option4"> Option 4 </option>
            </select> </td>
        </tr>
        <tr align=center>
            <td colspan=2>
                <br>
                <button type = "submit" class = "btn btn-primary text-white" style = "width:150px;font-family:verdana"> Add Question </button>
                &nbsp;
                &nbsp;
                &nbsp;
                <a href = "viewquestion.php?question_type_id=<?=$qtype_id?>&topic_id=<?=$topic_id?>" class = "btn btn-warning text-white" style = "width:150px;font-family:verdana"> Cancel </a>
            </td>
            

        </tr>
    </table>


</div>
</form>