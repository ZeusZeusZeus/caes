<?php
    include "header.php";
    if (!isset($_SESSION['admin_id'])){
        header("Location:index.php");
    }
    $user_id = $_GET['user_id'];
    $topic_id = $_GET['topic_id'];
    $topicname = "Select Topic_Name from topics where Topic_ID = '$topic_id'";
    $topicnamequery = custom_query($topicname);
    foreach($topicnamequery as $key =>$row){
        $TOPIC = $row['Topic_Name'];
    }
    $get = "SELECT * from users where user_id = '$user_id'";
    $getquery = custom_query($get);
    foreach($getquery as $key =>$row){
        $id = $row['user_id'];
        $fullname = $row['Firstname']." ". $row['Lastname'];
    }
?>

<div class = "container" style = "width:60%;margin:0 auto;border:none;">
    <div class = "card" style = "font-size:20px;">
    <div class = "card-header bg-primary text-white" style = "font-family:georgia;">
        <h1 align=center> <?=$fullname?>'s Quiz Records </h1>
    </div>
    <br>
    <h1 align=center> <?=$TOPIC?> </h1>
    <br>
    <div class = "row">
    <?php
        $qtype = "SELECT * from questions_type";
        $qtypequery = custom_query($qtype);
        foreach($qtypequery as $key =>$row){
            $Qtype_ID = $row['Question_Type_ID'];
            $Question_Type = $row['Question_Type'];
        ?>
            
            <div class = "card" style = "width:40%;margin:0 auto;font-family:verdana;">
                <div class = "card-header bg-info" style = "text-align:center;font-weight:bold;">
                    <?=$Question_Type?>
                </div>
                <div class = "card-body" style = "text-align:center">
                    <a href = "viewrecords.php?user_id=<?=$user_id?>&qtype_id=<?=$Qtype_ID?>&topic_id=<?=$topic_id?>" class = "btn btn-primary"><i class="far fa-eye"></i>  View </a>
                </div>
            </div>
            
            <br>
        <?php
        }
        ?>
    </div>
    <br>
    </div>
 
    <h1 align =center> <a href= "viewplayer.php?user_id=<?=$user_id?>" class = "btn btn-warning" style ="width:150px;"><i class = "fas fa-arrow-left"> Back </i> </a> </h1>
</div>