<?php
    include "header2.php";
    if (!isset($_SESSION['user_id'])){
        header("Location:index.php");
    }

?>
<style>
    
    tr{cursor: pointer; transition: all .25s ease-in-out}
    .selected{background-color: red; color: #fff;}
    
</style>

<div class = "container" style = "width:60%; margin:0 auto;border:none;">
    <div class = "card">
        <div class = "card-header bg-primary text-light">
            <h1 align=center > My Quiz History </h1>
        </div>
    <br>
        <div class = "card-body">
        <table class = "table" id = "table" role = "grid" style = "font-family:verdana;">
            <thead class = "thead" style = "font-size:20px;">
                <tr>
                    <th>
                        Name
                    </th>
                    <th>
                        Quiz Topic
                    </th>
                    <th>
                        Question Type
                    </th>
                    <th>
                        SCORE
                    </th>
                    <th>
                        DATE
                    </th>
                </tr>
            </thead>
            <?php
                $query = "SELECT quiz.user_id,Firstname, Lastname,Topic_Name, Question_Type, Time, score from quiz join users on users.user_id = quiz.user_id join topics on quiz.topic_id = topics.Topic_ID join questions_type on quiz.Question_Type_ID = questions_type.Question_Type_ID join results on quiz.quiz_id = results.quiz_id where quiz.user_id = '$user_id' ORDER by Time DESC";
                $quiz = custom_query($query);
                foreach($quiz as $key => $row){
                    $Fullname = $row['Firstname']." ".$row['Lastname'];
                    $Topic = $row['Topic_Name'];
                    $Qtype = $row['Question_Type'];
                    $score = $row['score'];
                    $date = $row['Time'];
                    $newdate= strtotime($date);  
                ?>
                <tr>
                    <td style = "font-weight:bold;">
                        <?=$Fullname?>
                    </td>
                    <td>
                        <?=$Topic?>
                    </td>
                    <td>
                        <?=$Qtype?>
                    </td>
                    <td>
                        <?=$score?>
                    </td>
                    <td>
                    <?=date('h:i a M d, Y ', $newdate)?>
                    </td>
                </tr>
                    
            <?php
            }
            ?>
        </table>
    </div>
    </div>
</div>
<script>
            
    function selectedRow(){
        
        var index,
            table = document.getElementById("table");
    
        for(var i = 1; i < table.rows.length; i++)
        {
            table.rows[i].onclick = function()
            {
                    // remove the background from the previous selected row
                if(typeof index !== "undefined"){
                    table.rows[index].classList.toggle("selected");
                }
                console.log(typeof index);
                // get the selected row index
                index = this.rowIndex;
                // add class selected to the row
                this.classList.toggle("selected");
                console.log(typeof index);
                };
        }
        
    }
    selectedRow();
</script>