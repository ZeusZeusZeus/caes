<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="assets/img/brain.png">
    <title>Online Quiz</title>
</head>
<body style = "background-color:#F0F6F7FF;">
    <?php
        include "header.php";
        if (!isset($_SESSION['admin_id'])){
            header("Location:index.php");
        }
    ?>  
     
        <div class = "card" style = "width:50%;margin:0 auto;">

            <div class = "card-header bg-primary text-light" style = "font-family:verdana;">
                <h1 align=center> Leaderboard  </h1>
            </div>
            <br>
            <div class = "card-body">
            <table  class = "table" style = "font-family:verdana;">
                <thead class = "thead" style = "font-size:20px;">
                    <tr align=center>
                        <th>
                            TOPIC
                        </th>
                        <th>
                        Question Type
                        </th>
                        <th>
                            ACTION
                        </th>
                    </tr>
                </thead>

        <?php
            $topic = "Select * from topics";
            $topicquery = custom_query($topic);

            foreach($topicquery as $key =>$row){
                $topic_id = $row['Topic_ID'];
                $topic_name = $row['Topic_Name'];


                $qtype = "Select * from questions_type";
                $qtypequery = custom_query($qtype);
                foreach($qtypequery as $key =>$row){
                    $qtype_id = $row['Question_Type_ID'];
                    $question_type = $row['Question_Type'];

                ?>
                    <tr align=center>
                        <td style = "font-size:18px">
                            <?=$topic_name?>
                        </td>
                        <td>
                            <?=$question_type?>
                        </td>
                        <td>
                            <a href = "viewleaderboard.php?qtype_id=<?=$qtype_id?>&topic_id=<?=$topic_id?>" class = "btn btn-primary text-white"><i class="far fa-eye"></i> View  </a>
                        </td>
                    </tr>  
                
            <?php
                } 
            
            }

        ?>
            </table>
            </div>
        </div>
    
</body>
</html>