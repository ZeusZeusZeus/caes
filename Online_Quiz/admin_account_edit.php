<?php
    include "header.php";
    if (!isset($_SESSION['admin_id'])){
        header("Location:index.php");
    }
    $table = "admin";
    $id = $_GET['id'];

    $data = get_where2($table, $id);

    foreach($data as $key =>$row){
        $firstname = $row['Firstname'];
        $lastname = $row['Lastname'];
        $email = $row['email'];
        $username = $row['username'];
        $password = $row['password'];
    }



?>

<div class ="container" style = "width:60%;margin:0 auto;border:none;">
    <h1 align=center> Account Setting </h1>
    <div class="row centered-form" style = "width:80%;margin:0 auto;">
        <div class="col-sm-12" style = "margin:20 auto;">
        <br>
        	<div class="panel panel-default" style = "font-family:verdana;font-size:20px;">
        		<div class="panel-heading" style = "font-family:Georgia;">
			    		
			 	</div>
			 			<div class="panel-body">
			    		<form role="form" action = "update_admin_account.php?id=<?=$id?>" method = "POST">
			    			<div class="row">
								<div class="col-md-2">
			    					<div class="form-group">
			                            <label> First Name </label>
			    					</div>
			    				</div>
			    				<div class="col-md-10">
			    					<div class="form-group">
			                            <input type="text" name="firstname" id="first_name" class="form-control input-lg" value = "<?=$firstname?>" required autocomplete = "off">
			    					</div>
			    				</div>
								<div class="col-md-2">
			    					<div class="form-group">
			                            <label> Last Name </label>
			    					</div>
			    				</div>
			    				<div class="col-md-10">
			    					<div class="form-group">
			    						<input type="text" name="lastname" id="last_name" class="form-control input-lg" value = '<?=$lastname?>'  required autocomplete = "off">
			    					</div>
			    				</div>
			    			
								<div class="col-md-2">
									<div class="form-group">
										<label> Email</label>
									</div>
								</div>
								<div class="col-md-10">
									<div class="form-group">
										<input type="email" name="email" id="email" class="form-control input-lg" value = "<?=$email?>" required autocomplete = "off">
									</div>
								</div>
								<div class="col-md-2">
									<div class="form-group">
										<label> Username</label>
									</div>
								</div>
								<div class="col-md-10">
									<div class="form-group">
										<input type="username" name="username" id="username" class="form-control input-lg" value = "<?=$username?>" required autocomplete = "off">
									</div>
								</div>
								<div class="col-md-2">
									<div class="form-group">
										<label> Password</label>
									</div>
								</div>
								<div class="col-md-10">
									<div class="form-group" >
										<input type="password" name="password" id="password" class="form-control input-lg" value = "<?=$password?>" required autocomplete = "off">
									</div>
								</div>
							</div>
                            
			    		
			    			
			    			<button type="submit" class="btn btn-info btn-block form-control input-lg"> Update </button>
                            <a href = "admin_account_info.php?id=<?=$id?>" class="btn btn-warning btn-block form-control input-lg"> Cancel </a>
			    		
			    		</form>
			    	</div>
	    		</div>
    		</div>
    	</div>
   

</div>